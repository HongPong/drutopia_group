## Groups
Each group has a page within the website. A group can launch a campaign, call for an action and publish a news article. All of these pieces of content are displayed on a group’s page, creating a mini site.

### Potential Future Features
* Group membership
* Group only content
* Integration with other tools such as: Discourse, Facebook, Loomio, Slack, Twitter